<?php

namespace App\Http\Controllers;

use App\shorturl;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Routing\UrlGenerator;

class ShortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shorts = shorturl::OrderBy('id','desc')->get();
        return view('index')->with('shorts',$shorts);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $charset = "abcdefghijklmnopqrstuvwxyz";
//        $num = "0123456789";
//        $url = "";
//
//        for($i=0; $i<5; $i++){
//            $url = $url . $num[rand(0,9)];
//        }
//        $url = $url . $charset[rand(0,strlen($charset))-1];

        $url = (rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)));


        $this->validate($request,
            [
            'long_url' => 'required'
        ]);
        $todo = new shorturl();
        $todo->long_url = $request->input('long_url');
        $todo->short_url = $url;
        $todo->view = 0;
        $todo->save();

        return redirect('/')->with('success','Created ShortUrl Successful : http://www.short.local/'.$url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorturl)
    {

        //dd('this is show function'.$id);
        //$check = shorturl::find($id);
        //return view('check')->with('check',$check);

        $shorts = shorturl::all();
        if(count($shorts)>0){
            foreach ($shorts as $short){
                if($short->short_url == $shorturl){
                    $short->view += 1;
                    $short->save();
                    return view('direct')->with('long_url',$short->long_url);
                }
            }
        }
        //return view('checkfail');
        return redirect('/')->with('fail','ไม่พบรหัส SHORT URL');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
