<?php if(session('success')): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo e(session ('success')); ?>

        <button type="button" class="close" data-dismiss="alert">
            <span>x</span>
        </button>
    </div>
<?php endif; ?>

<?php if(session('fail')): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <?php echo e(session ('fail')); ?>

        <button type="button" class="close" data-dismiss="alert">
            <span>x</span>
        </button>
    </div>
<?php endif; ?>

<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/inc/message.blade.php ENDPATH**/ ?>