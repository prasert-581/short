@extends('layouts.app')
@section('content')
    <br>


    <form method="post" action="{{ url('/') }}">
        @csrf
        <br>
        <h1 style="text-align: center;">Create new ShortURL</h1>
        <br>
        <div class="container p-5" style="border-style: solid;
                                        border-color: slategrey;
                                        border-radius: 20px;
                                        border-width: 2px;">

            <div class="row justify-content-md-center">
                {{--            <input type="text" name="title" class="form-control" value="{{ old('title') }}">--}}
                <div class="col-10">
                    <input type="text" name="long_url" class="form-control text-center" placeholder="PASTE LONG URL">
                </div>
                <br>
                <br>
                <br>
                <button type="submit" class="btn btn-danger col-4">CREATE SHORT URL</button>

            </div>

        </div>
    </form>
    <br>
    <form method="get" action="{{ url('/') }}">
        <button type="submit" class="btn btn-outline-primary col-2 p-2" style="margin-left: 20px;">BACK</button>
    </form>

@endsection
