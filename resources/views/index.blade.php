@extends('layouts.app')
@section('content')

    <br>
    <br>
    <h1 style="text-align:center; font-family: 'Prompt SemiBold';">SHORTEN URL WEBSITE</h1>

    <form method="get" action="{{ url('/new') }}">
        <button type="submit"   class="btn btn-outline-danger p-3" style="float: right;">Creat ShortUrl</button>
    </form>


    <br>
    <br>
    <br>


{{--            <div>--}}
{{--                <a href="{{url('/todo/'.$todo->id)}}">--}}
{{--                </a>--}}
{{--                <br>--}}
{{--                <p>{{$todo->created_at}}</p>--}}
{{--                <p>{{$todo->long_url}}</p>--}}
{{--                <p>{{$todo->short_url}}</p>--}}
{{--                <p>{{$todo->view}}</p>--}}
{{--            </div>--}}
{{--            <hr>--}}


{{--            <table class="table">--}}
{{--                <thead class="thead-dark">--}}
{{--                <tr>--}}

{{--                    <th scope="col" style=" font-family: 'Prompt SemiBold';">CREATED AT</th>--}}
{{--                    <th scope="col" style=" font-family: 'Prompt SemiBold';">LONG URL</th>--}}
{{--                    <th scope="col" style=" font-family: 'Prompt SemiBold';">SHORT URL</th>--}}
{{--                    <th scope="col" style=" font-family: 'Prompt SemiBold';"></th>--}}
{{--                    <th scope="col" style=" font-family: 'Prompt SemiBold';">VIEW</th>--}}

{{--                </tr>--}}
{{--                </thead>--}}
{{--                @if(count($shorts)>0)--}}
{{--                    @foreach($shorts as $short)--}}
{{--                <tbody>--}}
{{--                <tr class="table-light">--}}

{{--                    <td style=" font-family: 'Prompt SemiBold';">{{$short->created_at}}</td>--}}
{{--                    <td style=" font-family: 'Prompt SemiBold';"> <a href="{{ url($short->long_url) }}"> {{$short->long_url}}--}}
{{--                        </a>--}}
{{--                    </td>--}}
{{--                    <td style=" font-family: 'Prompt SemiBold';"> <input type="text" name="long_url" id="shortUrl{{$short->id}}" class="form-control col-12" value= " http://www.short.local/t/{{$short->short_url}}">   </td>--}}
{{--                    <td style=" font-family: 'Prompt SemiBold';"><button type="submit" onclick="copy(this)" value="{{$short->id}}" id="copyBtn" class="btn btn-outline-info" style="float: left;">COPY</button></td>--}}
{{--                    <td class="text-center" style=" font-family: 'Prompt SemiBold';">{{$short->view}}</td>--}}

{{--                </tr>--}}
{{--                </tbody>--}}
{{--                    @endforeach--}}
{{--                @endif--}}
{{--            </table>--}}



    <div class="container-fluid p-2 m-3 text-center" style="border-style: solid;
                                        border-color: slategrey;
                                        border-radius: 10px;
                                        border-width: 2px;
                                        background: #0c5460;
                                        color: whitesmoke;

">
        <div class="row text-center">
            <div class="col-2">CREATED AT</div>
            <div class="col-3"> LONG-URL </div>
            <div class="col-4"> SHORT-URL </div>
            <div class="col-1"></div>
            <div class="col-2">VIEW </div>
        </div>
    </div>

    @if(count($shorts)>0)
        @foreach($shorts as $short)
    <div class="container-fluid p-5 m-3 " style="border-style: solid;
                                        border-color: slategrey;
                                        border-radius: 20px;
                                        border-width: 2px;">
        <div class="row ">
            <div class="col-2" style=" font-family: 'Prompt SemiBold';">{{$short->created_at}}</div>
            <div class="col-4 text-center" style=" font-family: 'Prompt SemiBold';"> <a href=" {{ url($short->long_url) }}"> {{$short->long_url}}</a></div>
            <div class="col-4" style=" font-family: 'Prompt SemiBold';"><input type="text" name="long_url" id="shortUrl{{$short->id}}" class="form-control col-12" value= " http://www.short.local/t/{{$short->short_url}}">   </div>
            <div class="col-1" style=" font-family: 'Prompt SemiBold'; font-size: 20px;"><button type="submit" onclick="copy(this)" value="{{$short->id}}" id="copyBtn" class="btn btn-outline-info" style="float: left;">COPY</button></div>
            <div class="col-1 text-center" style=" font-family: 'Prompt SemiBold'; font-size: 25px;">{{$short->view}}</div>
        </div>
    </div>
    @endforeach
   @endif


<script>
    function copy(clickCopy) {
        {{--/* Get the text field */--}}
        {{--var copyText = document.getElementById("shortUrl{{$short->id}}");--}}

        {{--/* Select the text field */--}}
        {{--copyText.select();--}}
        {{--copyText.setSelectionRange(0, 99999); /*For mobile devices*/--}}

        {{--/* Copy the text inside the text field */--}}
        {{--document.execCommand("copy");--}}

        {{--/* Alert the copied text */--}}
        {{--alert("Copied the text: " + copyText.value);--}}

           var id = clickCopy.value;
           var copyText = document.querySelector("#shortUrl"+id);
           copyText.select();
           copyText.setSelectionRange(0, 99999);
           document.execCommand("copy");
           alert("Copied the text: " + copyText.value);
    }


</script>


@endsection
